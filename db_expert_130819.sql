-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2019 at 12:05 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_expert`
--

-- --------------------------------------------------------

--
-- Table structure for table `ds_category`
--

CREATE TABLE `ds_category` (
  `id_category` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds_category`
--

INSERT INTO `ds_category` (`id_category`, `category_name`) VALUES
(1, 'Hama'),
(2, 'Penyakit');

-- --------------------------------------------------------

--
-- Table structure for table `ds_evidences`
--

CREATE TABLE `ds_evidences` (
  `id` int(11) NOT NULL,
  `code` varchar(3) DEFAULT NULL,
  `name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ds_evidences`
--

INSERT INTO `ds_evidences` (`id`, `code`, `name`) VALUES
(1, 'G1', 'Daun muda belum membuka'),
(2, 'G2', 'Pangkal daun berlubang'),
(3, 'G3', 'Daun menggulung'),
(4, 'G4', 'Tumbuh tegak'),
(5, 'G5', 'Daun menguning'),
(36, 'G27', 'Pelepah berukuran kecil'),
(6, 'G6', 'Daun kering'),
(7, 'G7', 'Daun berlubang'),
(8, 'G8', 'Daun berlubang seperti jaring'),
(9, 'G9', 'Daun berwarna abu-abu'),
(10, 'G10', 'Bekas gigitan pada buah'),
(37, 'G28', 'Terdapat miserium putih diantara buah'),
(11, 'G11', 'Pertumbuhan tidak normal'),
(12, 'G12', 'Daun rusak pada bagian tepi'),
(13, 'G13', 'Daun berwarna seperti perunggu'),
(14, 'G14', 'Daun mengkilat'),
(15, 'G15', 'Buah berlubang'),
(16, 'G16', 'Daun patah'),
(35, 'G26', 'Daun sobek pada bagian tengah'),
(17, 'G17', 'Kuncup mengeluarkan bau busuk'),
(18, 'G18', 'Kuncup membusuk'),
(19, 'G19', 'Mudah dicabut'),
(20, 'G20', 'Daun berwarna coklat'),
(21, 'G21', 'Batang atas membusuk'),
(22, 'G22', 'Batang berwarna coklat keabuan'),
(23, 'G23', 'Daun terbawah berubah warna'),
(24, 'G24', 'Daun berwarna hijau pucat'),
(25, 'G25', 'Mengeluarkan getah'),
(26, 'G26', 'Daun tua layu'),
(27, 'G27', 'Daun tua patah'),
(28, 'G28', 'Tandan buah membusuk'),
(29, 'G29', 'Pelepah daun patah'),
(30, 'G30', 'Daun berwarna kuning'),
(31, 'G31', 'Tanaman layu'),
(32, 'G32', 'Bercak coklat pada ujung & tepi daun'),
(33, 'G33', 'Bercak coklat dikelilingi warna kuning'),
(34, 'G34', 'Bercak kuning pada daun');

-- --------------------------------------------------------

--
-- Table structure for table `ds_history`
--

CREATE TABLE `ds_history` (
  `id_history` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `evidence` varchar(255) NOT NULL,
  `problem` varchar(100) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds_history`
--

INSERT INTO `ds_history` (`id_history`, `nama`, `nik`, `alamat`, `evidence`, `problem`, `waktu`) VALUES
(1, 'Dwi Susanto', '13000111333', 'jogja', 'Batang atas membusuk, Batang berwarna coklat keabuan, Bekas gigitan pada buah', 'Busuk Batang Atas', '2019-08-02 16:08:47'),
(2, 'Tata', '14000222333', 'Riau', 'Bercak coklat pada ujung & tepi daun, Buah berlubang, Daun berlubang', 'Antraknosa', '2019-08-02 16:08:38'),
(3, 'Yayuk Sulistyowati ( Testing )', '23423424', 'Jogja', 'Bercak coklat pada ujung & tepi daun, Bercak kuning pada daun, Buah berlubang', 'Antraknosa', '2019-08-13 16:08:39'),
(4, 'Ketut Yulistyanigrum', '1999000990', 'Jogja', 'Bercak coklat pada ujung & tepi daun, Bercak kuning pada daun', 'Garis Kuning', '2019-08-13 16:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `ds_info`
--

CREATE TABLE `ds_info` (
  `id_info` int(11) NOT NULL,
  `nama_petani` varchar(255) NOT NULL,
  `evidence` text NOT NULL,
  `waktu` datetime NOT NULL,
  `nik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds_info`
--

INSERT INTO `ds_info` (`id_info`, `nama_petani`, `evidence`, `waktu`, `nik`) VALUES
(2, 'Tukiman', 'Bintik Hitam di batang', '2019-07-22 11:33:32', '133333'),
(3, 'Sarwoto', 'Daun sobek bagian belakang', '2019-07-22 11:52:27', '14141414'),
(4, 'Sutarno', 'Batang patah', '2019-07-22 14:39:16', '15125235'),
(5, 'Tes', 'Tes', '2019-07-22 18:40:16', '23423542'),
(6, 'Tata', 'Bercak merah', '2019-08-13 16:33:04', '140000');

-- --------------------------------------------------------

--
-- Table structure for table `ds_problems`
--

CREATE TABLE `ds_problems` (
  `id` int(11) NOT NULL,
  `code` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `id_category` int(11) NOT NULL,
  `solusi` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ds_problems`
--

INSERT INTO `ds_problems` (`id`, `code`, `name`, `id_category`, `solusi`) VALUES
(1, 'P1', 'Kumbang', 1, 'Menggunakan predator seperti ular, burung dan sebagainya. Selain menggunakan predator hama juga dapat menggunakan parasit hama tersebut seperti virus Baculovirus oryctes dan jamur seperti Metharrizium anisopliae.'),
(2, 'P2', 'Nematoda', 1, 'Pohon yang terserang dibongkar dan dibakar, ataupun dengan cara tanaman dimatikan dengan menggunakan racun natrium arsenit.'),
(3, 'P3', 'Ulat API', 1, 'Pengaplikasian insektisida berbahan aktif triazofos 242 gr/lt, karbaril 85 % dan klorpirifos 200 gr/lt.'),
(4, 'P4', 'Ulat Kantong', 1, 'Aplikasi insektisida yang berbahan aktif triklorfon 707 gr/lt dengan dosis 1.5 â€“ 2 kg/ha. Dapat juga menggunakan timah arsetat dengan dosis 2.5 kg/ha.'),
(5, 'P5', 'Tikus', 1, 'Menggunakan predator seperti burung hantu, ular dan sebagainya, serta tindakan pengemposan pada tempat-tempat yang dijadikan sarang oleh tikus.'),
(6, 'P6', 'Belalang', 1, 'Menggunakan predator seperti burung sebagai pemangsa alaminya.'),
(7, 'P7', 'Tungau', 1, 'Aplikasi akarisida yang mengandung bahan aktif tetradifon 75.2 gr/lt.'),
(8, 'P8', 'Ngengat', 1, 'Pengaplikasian insektisida yang mengandung bahan aktif triklorfon 707 gr/lt atau andosulfan 350 gr/lt.'),
(9, 'P9', 'Phimelephila Ghesquiesel', 1, 'Pengaplikasian semprot parathion 0.02 %. '),
(10, 'P10', 'Busuk Titik Tumbuh / Busuk Akar', 2, 'Mengaplikasikan bakteri yang berfungsi sebagai pemangsa bagi bakteri erwinia.'),
(11, 'P11', 'Busuk Kuncup', 2, 'Melakukan pemotongan bagian kuncup yang terserang.'),
(12, 'P12', 'Busuk Batang Atas', 2, 'Dengan cara membuang bagian batang yang terserang dan menutup bekas luka dengan obat luka yang ada. Pada kondisi parah tanaman dibongkar dan dimusnahkan.'),
(13, 'P13', 'Busuk Pangkal Batang', 2, 'Dapat melakukan aplikasi dengan menggunakan bahan yang mengandung Tricodherma ( produk CustomBio ), dapat disemprotkan kebagian yang terserang dan penyemprotan pada tanah sekeliling tanaman pokok secara melingkar. '),
(14, 'P14', 'Busuk Kering Pangkal Batang', 2, 'Untuk tanaman yang sudah terserang secara hebat dengan melakukan pembongkaran dan pemusnahan dengan cara dibakar.'),
(15, 'P15', 'Penyakit Akar', 2, 'Dimulai sejak awal kegiatan di dalam pesemaian dengan mempersiapkan media yang tidak terkontaminasi jamur, drainase yang baik agar tidak terjadi kekeringan yang ekstrim pada tanaman.'),
(16, 'P16', 'Antraknosa', 2, 'Sejak awal mulai dari pemindahan bibit, dimana seluruh media tanah bibit disertakan, jarak tanam, penyiraman dan pemupukan yang dilakukan secara teratur dan berimbang, aplikasi Captan 0.2 % atau Cuman 0.1 %'),
(17, 'P17', 'Garis Kuning', 2, 'Melakukan proses inokulasi pada bibit dan tanaman muda, atau dengan melakukan aplikasi bahan yang mengandung Tricodherma & Bacillus ( produk CustomBio )'),
(18, 'P18', 'Penyakit Tajuk', 2, 'Dimulai sejak awal terutama melakukan seleksi indukan yang bersifat karier penyakit ini, sehingga akan didapatkan bibit yang mempunyai sifat-sifat yang sehat.'),
(19, 'P19', 'Busuk Tandan', 2, 'Menjaga sanitasi kebun terutama pada musim penghujan, aplikasi difolatan 0.2 %, melakukan penyerbukan buatan atau kastrasi.');

-- --------------------------------------------------------

--
-- Table structure for table `ds_rules`
--

CREATE TABLE `ds_rules` (
  `id` int(11) NOT NULL,
  `id_problem` int(11) DEFAULT NULL,
  `id_evidence` int(11) DEFAULT NULL,
  `cf` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ds_rules`
--

INSERT INTO `ds_rules` (`id`, `id_problem`, `id_evidence`, `cf`) VALUES
(1, 1, 1, 0.4),
(2, 1, 2, 0.3),
(3, 2, 3, 0.2),
(4, 2, 4, 0.6),
(5, 2, 5, 0.5),
(6, 2, 6, 0.8),
(7, 3, 7, 0.3),
(8, 4, 9, 0.7),
(9, 4, 8, 0.3),
(10, 4, 6, 0.8),
(11, 5, 10, 0.1),
(12, 5, 11, 0.4),
(13, 6, 12, 0.3),
(14, 7, 13, 0.6),
(15, 7, 14, 0.3),
(16, 8, 15, 0.2),
(17, 9, 16, 0.8),
(18, 10, 17, 0.6),
(19, 10, 18, 0.7),
(20, 10, 19, 0.8),
(21, 11, 20, 0.8),
(22, 11, 18, 0.7),
(23, 12, 21, 0.4),
(24, 12, 22, 0.7),
(25, 12, 23, 0.6),
(26, 13, 24, 0.2),
(27, 13, 25, 0.6),
(28, 13, 26, 0.5),
(29, 13, 27, 0.8),
(30, 14, 28, 0.2),
(31, 14, 29, 0.8),
(32, 15, 11, 0.4),
(33, 15, 30, 0.5),
(34, 15, 31, 0.5),
(35, 16, 32, 0.6),
(36, 16, 33, 0.6),
(37, 17, 34, 0.6),
(38, 18, 35, 0.3),
(39, 18, 36, 0.4),
(40, 19, 37, 0.9);

-- --------------------------------------------------------

--
-- Table structure for table `ds_user`
--

CREATE TABLE `ds_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ds_user`
--

INSERT INTO `ds_user` (`id_user`, `username`, `password`, `nama`, `level`) VALUES
(1, 'pakar_sistem', '25d55ad283aa400af464c76d713c07ad', 'Pakar Tata', 'pakar'),
(2, 'admin_sistem', '25d55ad283aa400af464c76d713c07ad', 'Super Admin Tata', 'super_admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ds_category`
--
ALTER TABLE `ds_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `ds_evidences`
--
ALTER TABLE `ds_evidences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds_history`
--
ALTER TABLE `ds_history`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `ds_info`
--
ALTER TABLE `ds_info`
  ADD PRIMARY KEY (`id_info`);

--
-- Indexes for table `ds_problems`
--
ALTER TABLE `ds_problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds_rules`
--
ALTER TABLE `ds_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ds_user`
--
ALTER TABLE `ds_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ds_category`
--
ALTER TABLE `ds_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ds_evidences`
--
ALTER TABLE `ds_evidences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `ds_history`
--
ALTER TABLE `ds_history`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ds_info`
--
ALTER TABLE `ds_info`
  MODIFY `id_info` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ds_problems`
--
ALTER TABLE `ds_problems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ds_rules`
--
ALTER TABLE `ds_rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `ds_user`
--
ALTER TABLE `ds_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
