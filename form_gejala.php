<?php 
include ("lib/koneksi.php");
include ("header.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Sistem Pakar
		</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Form Gejala</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<form name="tambah" role="form" action="submit_form.php" method="post" >
							<div class="form-group">
								<label>Nama Petani</label>
								<input type="text" class="form-control" placeholder="Nama Petani ..." name="nama_petani" required>
							</div>							
							<div class="form-group">
								<label>NIK</label>
								<input type="number" class="form-control" placeholder="NIK..." name="nik" required>
							</div>
							<div class="form-group">
								<label>Gejala</label>
								<textarea class="form-control" name="evidence"></textarea>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				Tata UAD 2019
			</div>
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	<strong>Tata &copy; 2019 | Universitas Ahmad Dahlan
	</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("footer.php");
?>
