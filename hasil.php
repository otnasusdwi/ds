<?php 
include ("lib/koneksi.php");
session_start();
include ("header.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Sistem Pakar
		</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Hasil Diagnosa</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Rank</th>
									<th>Hama-Penyakit</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								$densitas_baru = $_SESSION["densitas_baru"];

								$codes=array_keys($densitas_baru);

								$data = mysqli_query ($koneksi, "SELECT * FROM ds_problems WHERE code = '$codes[0]'");
								while($row = mysqli_fetch_array($data)){
									$penyakit = $row['name'];
									$solusi = $row['solusi'];
									$nilai = round($densitas_baru[$codes[0]]*100,2);
								}

								for ($i=0; $i < count($codes); $i++) { 
									$code = $codes[$i];
									$data = mysqli_query ($koneksi, "SELECT * FROM ds_problems WHERE code = '$code'");
									while($row = mysqli_fetch_array($data)){ ?>
										<tr>
											<td><?php echo $i+1; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo round($densitas_baru[$codes[$i]]*100,2) ?> %</td>
										</tr>
										<?php $no++;}  ?>
									<?php }  ?>
								</tbody>
							</table>
						</div>
						<br>
						<hr>
						<h4><p style="text-align:center">Terdeteksi penyakit <b><?php echo $penyakit; ?></b> dengan derajat kepercayaan yang paling tinggi yaitu <b><?php echo $nilai; ?> %</b></p></h4>
						<hr>
						<h4 style="padding-left:10%; padding-right: 10%; "><p style="text-align:center"><b>Solusi Pengendalian : </b><br><?php echo $solusi; ?></p></h4>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					Tata UAD 2019
				</div>
			</div>
			<!-- /.box -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<strong>Tata &copy; 2019 | Universitas Ahmad Dahlan
		</footer>
	</div>
	<!-- ./wrapper -->
	<?php 
	include ("footer.php");
	?>