<?php 
include ("lib/koneksi.php");
include ("header.php");
$nama = $_GET['nama'];
$alamat = $_GET['alamat'];
$nik = $_GET['nik'];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Sistem Pakar
		</h1>
		<ol class="breadcrumb">
			<li><a href="konsultasi.php"><i class="fa fa-dashboard"></i> Konsultasi</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Pilih Gejala Yang Ada Pada Tanaman Kelapa Sawit Anda !</h3>
			</div>
			<!-- /.box-header -->
			<form name="tambah" role="form" action="diagnosa.php" method="post" >
				<input type="hidden" name="nama" value="<?php echo $nama; ?>">
				<input type="hidden" name="alamat" value="<?php echo $alamat; ?>">
				<input type="hidden" name="nik" value="<?php echo $nik; ?>">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<?php
								include ("lib/connect.php");
								$sql="SELECT * FROM ds_evidences ORDER BY name ASC";
								$result=$db->query($sql);
								while($row=$result->fetch_object()){
									?>
									<div class="form-group">
										<label>
											<input type="checkbox" class="flat-red" name="evidence[]" value="<?php echo $row->id; ?>">
											&nbsp; <?php echo $row->name; ?>
										</label>
									</div>
								<?php } ?>
							</div>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<p align="center">
						<button type="submit" class="btn btn-primary">Diagnosa</button>
					</p>
				</div>
			</form>
			<div class="box-footer">
				<p align="center">
					<h4 style="text-align: center;">Tidak menemukan gejala / masalah dilapangan ? <a href="form_gejala.php"><b>Klik disini</b></a><br> untuk mengisi form yang akan dikirim ke Pakar untuk update Sistem Pakar</h4>
				</p>
			</div>
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	<strong>Tata &copy; 2019 | Universitas Ahmad Dahlan
	</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("footer.php");
?>