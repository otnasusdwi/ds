<?php 
include ("lib/connect.php");
session_start();
if(isset($_POST['evidence'])){ //memproses jika gejala sudah dipilih
	$nama = $_POST['nama'];
	$nik = $_POST['nik'];
	$alamat = $_POST['alamat'];
	$gejala = $_POST['evidence'];
	//5-8 menangkap value nama, nik, alamat, dan gejala dari form sebelumnya
	
	//Mengambil Nilai Gejala yang Dipilih
	$sql = "SELECT GROUP_CONCAT(b.code), a.cf
	FROM ds_rules a
	JOIN ds_problems b ON a.id_problem=b.id
	WHERE a.id_evidence IN(".implode(',',$_POST['evidence']).") 
	GROUP BY a.id_evidence";
	$result=$db->query($sql);
	$evidence=array();
	while($row=$result->fetch_row()){
		$evidence[]=$row;
	}
	//12-20 Pada saat form tersebut disubmit dengan menekan tombol proses maka data akan dikirim dengan HTTP POST method 

	//Menentukan environement
	$sql="SELECT GROUP_CONCAT(code) FROM ds_problems";
	$result=$db->query($sql);
	while($row=$result->fetch_row()){
		$fod=$row[0];
	}
	//25-28 fungsi GROUP_CONCAT() maka nilai dari field code akan ditampilkan dalam bentuk serialized string dengan koma (,) sebagai pemisah antar code tersebut. (dalam contoh aplikasi ini akan menjadi 'P1,P2,P3'), sehingga nilai dari variable $fod adalah merupakan environmen

	//menentukan nilai densitas
	$densitas_baru=array();
	while(!empty($evidence)){
		$densitas1[0]=array_shift($evidence); //Data gejala yang diterima ($evidence) akan diambil satu demi satu dengan fungsi array_shift() Gejala yang muncul, sesuai urutan akan dimasukkan ke dalam variabel $evidence[0], sedangkan plausability-nya dimasukkan ke variabel evidence[1]
		$densitas1[1]=array($fod,1-$densitas1[0][1]);
		$densitas2=array();
		if(empty($densitas_baru)){ //Jika belum ada hasil perhitungan densitas sebelumnya (empty($densitas_baru)) maka gejala berikutnya (yang kedua) dimasukkan ke dalam $densitas2[0] sebagai densitas kedua; 
			$densitas2[0]=array_shift($evidence);
		}else{ //jika sudah ada maka semua hasil perhitungan sebelumnya kecuali yang ber-index/key "&theta;" dimasukkan ke variabel array $densitas2 sebagai item array baru yang merepresentasikan suatu densitas baru
			foreach($densitas_baru as $k=>$r){
				if($k!="&theta;"){
					$densitas2[]=array($k,$r);
				}
			}
		}
		$theta=1; //menghitung nilai plausability dari densitas baru tersebut ($theta); yaitu 1 dikurangi jumlah total nilai kemungkinan dari masing-masing elemen densitas baru itu, dan meng-inisialisai variable $theta=1;
		foreach($densitas2 as $d) $theta-=$d[1]; //pengurangan nilai kemungkinan masing-masing elemen densitas yang baru 
		$densitas2[]=array($fod,$theta);//Hasilnya dimasukkan dalam item array baru dari variable array $densitas2.
		$m=count($densitas2); //menghitung jumlah iterasi, sesuai jumlah item array dari densitas 
		$densitas_baru=array();
		for($y=0;$y<$m;$y++){
			for($x=0;$x<2;$x++){
				if(!($y==$m-1 && $x==1)){ //pembatasan karena hasil perkalian antara nilai kemungkinan dari plausability dari densitas yang pertama dan kedua tidak diperhitungkan
					$v=explode(',',$densitas1[$x][0]);
					$w=explode(',',$densitas2[$y][0]);
					sort($v);
					sort($w);
					$vw=array_intersect($v,$w);
					if(empty($vw)){
						$k="&theta;";
					}else{
						$k=implode(',',$vw);
					}
					if(!isset($densitas_baru[$k])){
						$densitas_baru[$k]=$densitas1[$x][1]*$densitas2[$y][1];
					}else{
						$densitas_baru[$k]+=$densitas1[$x][1]*$densitas2[$y][1];
					}
					//65-69 Tiap elemen dari $densitas1 dan $densitas2 dikalikan nilai kemungkinannya, dan index/key-nya merupakan irisan --intersect dari kode penyakitnya (dalam hal ini memakai fungsi array_intersect()).
				}
			}
		}
		foreach($densitas_baru as $k=>$d){
			if($k!="&theta;"){
				$densitas_baru[$k]=$d/(1-(isset($densitas_baru["&theta;"])?$densitas_baru["&theta;"]:0));
				//proses perangkingan ini, pertama-tama hilangkan hasil sebelumnya yang mempunyai key/index="&theta;" dengan memakai kode unset($densitas_baru["&theta;"]);. 
			}
		}
	}

	//Perangkingan
	unset($densitas_baru["&theta;"]);
	arsort($densitas_baru); //Selanjunya data tersebut diurutkan dengan tetap mempertahankan associative keys-nya dengan pengurutan secara terbalik (reverse -- dari besar ke kecil) menggunakan fungsi arsort.

	$_SESSION["nama"] = $nama;
	$_SESSION["nik"] = $nik;
	$_SESSION["alamat"] = $alamat;
	$_SESSION["densitas_baru"] = $densitas_baru;
	$_SESSION["gejala"] = $gejala;
	//86-90 menyimpan ke session untuk dilakukan input history
	
	// header("location:hasil.php");
	header("location:input_history.php");
}else{
	header("location:konsultasi.php");
}
?>