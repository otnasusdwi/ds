<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4>
      Data Kategori
    </h4>
    <ol class="breadcrumb">
      <li><a href="kategori.php"><i class="fa fa-dashboard"></i> Kategori</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Data Kategori</h3> -->
        <!-- <a href="tambah_kategori.php" class="btn-success btn-sm">
          <i class="fa fa-plus"></i>&nbsp; Tambah
        </a> -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Kategori</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            $data = mysqli_query ($koneksi, "SELECT * FROM  ds_category ORDER BY id_category DESC");
            while($row = mysqli_fetch_array($data))
            {
             ?>
             <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row['category_name']; ?></td>
              <!-- <td>
                <a href="edit_kategori.php?id_category=<?php echo $row['id_category']; ?>" class="btn-warning btn-sm">
                  <i class="fa fa-edit"></i> Edit
                </a> &nbsp;
                <a href="javascript:confirmDelete('delete.php?id_category=<?php echo $row['id_category']; ?>')" class="btn-danger btn-sm">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td> -->
            </tr>
            <?php $no++;}  ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama Kategori</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Tata UAD 2019
      </div>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
</footer>
</div>
<!-- wrapper -->

<?php 
include ("../template/footer.php");
?>