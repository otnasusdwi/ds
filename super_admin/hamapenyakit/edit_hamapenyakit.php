<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Hama - Penyakit
		</h4>
		<ol class="breadcrumb">
			<li><a href="hamapenyakit.php"><i class="fa fa-dashboard"></i> Hama - Penyakit</a></li>
			<li class="active">Edit Hama - Penyakit</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Data Kategori</h3> -->
				Edit Hama - Penyakit
			</div>
			<!-- /.box-header -->
			<?php 
			$id = $_GET['id'];
			$data = mysqli_query ($koneksi, "SELECT * FROM ds_problems WHERE id = $id");
			while ($row = mysqli_fetch_array($data))
			{
				?>
				<form name="update" role="form" action="update.php" method="post" >
					<div class="box-body">
						<!-- text input -->
						<div class="form-group">
							<label>Kode</label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>">
							<input type="text" class="form-control" placeholder="Nama ..." name="code" value="<?php echo $row['code']; ?>" required>
						</div>
						<div class="form-group">
							<label>Nama</label>
							<input type="text" class="form-control" placeholder="Nama ..." name="name" value="<?php echo $row['name']; ?>" required>
						</div>
						<div class="form-group">
							<label>Kategori</label>								
							<select name="id_category" class="form-control">
								<?php 
								$datakategori = mysqli_query ($koneksi, "SELECT * FROM  ds_category ORDER BY id_category DESC");
								while($rowkategori = mysqli_fetch_array($datakategori))
								{
									?>
									<option value="<?php echo $rowkategori['id_category'] ?>" <?php	if($rowkategori['id_category'] == $row['id_category']){ echo "selected";}?>><?php echo $rowkategori['category_name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>					
			</form>
		<?php } ?>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->

<?php 
include ("../template/footer.php");
?>