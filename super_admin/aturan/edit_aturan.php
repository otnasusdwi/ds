<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Aturan
		</h4>
		<ol class="breadcrumb">
			<li><a href="hamapenyakit.php"><i class="fa fa-dashboard"></i> Aturan</a></li>
			<li class="active">Edit Aturan</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Data Kategori</h3> -->
				Edit Aturan
			</div>
			<!-- /.box-header -->
			<?php 
			$id_aturan = $_GET['id_aturan'];
			$data = mysqli_query ($koneksi, "SELECT * FROM aturan WHERE id_aturan = $id_aturan");
			while ($row = mysqli_fetch_array($data))
			{
				?>
				<form name="update" role="form" action="update.php" method="post" >
					<div class="box-body">
						<input type="hidden" class="form-control" name="id_aturan" value="<?php echo $row['id_aturan']; ?>">
						<div class="col-md-9">
							<div class="form-group">
								<label>Hama - Penyakit</label>								
								<select name="id_hamapenyakit" class="form-control">
									<?php 
									$datahama = mysqli_query ($koneksi, "SELECT * FROM aturan ORDER BY id_hamapenyakit DESC");
									while($rowhama = mysqli_fetch_array($datahama))
									{
										?>
										<option value="<?php echo $rowhama['id_hamapenyakit'] ?>" <?php	if($rowhama['id_hamapenyakit'] == $row['id_hamapenyakit']){ echo "selected";}?>><?php echo $rowhama['nama_hamapenyakit'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>					
			</form>
		<?php } ?>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>