<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4>
      Data Aturan
    </h4>
    <ol class="breadcrumb">
      <li><a href="kategori.php"><i class="fa fa-dashboard"></i> Aturan</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Data Kategori</h3> -->
        <!-- <a href="tambah_aturan.php" class="btn-success btn-sm">
          <i class="fa fa-plus"></i>&nbsp; Tambah
        </a> -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Hama - Penyakit</th>
              <th>Gejala</th>
              <th>Nilai Belief</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            $data = mysqli_query ($koneksi, 
              "SELECT ds_rules.id, ds_problems.name, ds_rules.cf, ds_evidences.name AS problem_name FROM ds_rules JOIN ds_evidences ON ds_rules.id_evidence = ds_evidences.id JOIN ds_problems ON ds_rules.id_problem = ds_problems.id ORDER BY ds_rules.id DESC;");
            while($row = mysqli_fetch_array($data))
            {
             ?>
             <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row['name']; ?></td>
              <td><?php echo $row['problem_name']; ?></td>
              <td><?php echo $row['cf']; ?></td>
              <!-- <td>
                <a href="javascript:confirmDelete('delete.php?id=<?php echo $row['id']; ?>')" class="btn-danger btn-sm">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td> -->
            </tr>
            <?php $no++;}  ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Hama - Penyakit</th>
              <th>Gejala</th>
              <th>Nilai Belief</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Tata UAD 2019
      </div>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>