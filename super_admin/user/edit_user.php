<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data User
		</h4>
		<ol class="breadcrumb">
			<li><a href="user.php"><i class="fa fa-dashboard"></i> User</a></li>
			<li class="active">Edit User</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				Edit User
			</div>
			<!-- /.box-header -->
			<?php 
			$id_user = $_GET['id_user'];
			$data = mysqli_query ($koneksi, "SELECT * FROM ds_user WHERE id_user = $id_user");
			while ($row = mysqli_fetch_array($data))
			{
				?>
				<form name="edit" role="form" action="update.php" method="post" >
					<div class="box-body">
						<!-- text input -->
						<div class="form-group">
							<label>Nama</label>
							<input type="hidden" name="id_user" value="<?php echo $row['id_user']; ?>">
							<input type="text" class="form-control" placeholder="Nama" name="nama" required value="<?php echo $row['nama']; ?>">
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" placeholder="Username" name="username" required value="<?php echo $row['username']; ?>">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="text" class="form-control" placeholder="Password" name="password" required value="<?php echo $row['password']; ?>">
						</div>
						<div class="form-group">
							<div class="radio">
								<label>
									<input type="radio" name="level" id="optionsRadios1" value="super_admin" <?php if ($row['level'] == 'super_admin'){ echo "checked"; } ?> >
									Admin
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="level" id="optionsRadios2" value="pakar" <?php if ($row['level'] == 'pakar'){ echo "checked"; } ?> >
									Pakar
								</label>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</form>
			<?php } ?>
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->


<?php 
include ("../template/footer.php");
?>
