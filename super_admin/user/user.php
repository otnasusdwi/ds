<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4>
      Data User
    </h4>
    <ol class="breadcrumb">
      <li><a href="user.php"><i class="fa fa-dashboard"></i> User</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Data user</h3> -->
        <a href="tambah_user.php" class="btn-success btn-sm">
          <i class="fa fa-plus"></i>&nbsp; Tambah
        </a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Password</th>
              <th>Level</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            $data = mysqli_query ($koneksi, "SELECT * FROM  ds_user ORDER BY id_user DESC");
            while($row = mysqli_fetch_array($data))
            {
             ?>
             <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row['nama']; ?></td>
              <td><?php echo $row['username']; ?></td>
              <td><?php echo $row['password']; ?></td>
              <td><?php echo $row['level']; ?></td>
              <td>
                <a href="edit_user.php?id_user=<?php echo $row['id_user']; ?>" class="btn-warning btn-sm">
                  <i class="fa fa-edit"></i> Edit
                </a> &nbsp;
                <a href="javascript:confirmDelete('delete.php?id_user=<?php echo $row['id_user']; ?>')" class="btn-danger btn-sm">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td>
            </tr>
            <?php $no++;}  ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Password</th>
              <th>Level</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Tata UAD 2019
      </div>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
</footer>
</div>
<!-- wrapper -->

<?php 
include ("../template/footer.php");
?>