<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data User
		</h4>
		<ol class="breadcrumb">
			<li><a href="user.php"><i class="fa fa-dashboard"></i> User</a></li>
			<li class="active">Tambah User</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				Tambah User
			</div>
			<!-- /.box-header -->
			<form name="tambah" role="form" action="add.php" method="post" >
				<div class="box-body">
					<!-- text input -->
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" placeholder="Nama" name="nama" required>
					</div>
					<div class="form-group">
						<label>Username</label>
						<input type="text" class="form-control" placeholder="Username" name="username" required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="text" class="form-control" placeholder="Password" name="password" required>
					</div>
					<div class="form-group">
						<div class="radio">
							<label>
								<input type="radio" name="level" id="optionsRadios1" value="super_admin" checked>
								Super Admin
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="level" id="optionsRadios2" value="pakar">
								Pakar
							</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				
			</form>
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->


<?php 
include ("../template/footer.php");
?>
