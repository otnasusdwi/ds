<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4>
      Data Form Gejala
    </h4>
    <ol class="breadcrumb">
      <li><a href="info.php"><i class="fa fa-dashboard"></i> Form Gejala</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-success">
      <div class="box-header with-border">
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama Petani</th>
              <th>Gejala / Masalah</th>
              <th>Waktu</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            $data = mysqli_query ($koneksi, "SELECT * FROM  ds_info ORDER BY id_info DESC");
            while($row = mysqli_fetch_array($data))
            {
             ?>
             <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row['nik']; ?></td>
              <td><?php echo $row['nama_petani']; ?></td>
              <td><?php echo $row['evidence']; ?></td>
              <td><?php echo $row['waktu']; ?></td>
              <!-- <td>
                <a href="javascript:confirmDelete('delete.php?id_info=<?php echo $row['id_info']; ?>')" class="btn-danger btn-sm">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td> -->
            </tr>
            <?php $no++;}  ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama Petani</th>
              <th>Gejala / Masalah</th>
              <th>Waktu</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Tata UAD 2019
      </div>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
</footer>
</div>
<!-- wrapper -->

<?php 
include ("../template/footer.php");
?>