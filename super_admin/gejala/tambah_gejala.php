<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Gejala
		</h4>
		<ol class="breadcrumb">
			<li><a href="gejala.php"><i class="fa fa-dashboard"></i> Gejala</a></li>
			<li class="active">Tambah Gejala</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Data Kategori</h3> -->
				Tambah Gejala
			</div>
			<!-- /.box-header -->
			<form name="tambah" role="form" action="add.php" method="post" >
				<div class="box-body">
					<!-- text input -->
					<div class="form-group">
						<label>Kode</label>
						<input type="text" class="form-control" placeholder="Kode Gejala ..." name="code" required>
					</div>
					<div class="form-group">
						<label>Nama Gejala</label>
						<input type="text" class="form-control" placeholder="Nama Gejala ..." name="name" required>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>					
		</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>