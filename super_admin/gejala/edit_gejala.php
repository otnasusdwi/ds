<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Kategori
		</h4>
		<ol class="breadcrumb">
			<li><a href="kategori.php"><i class="fa fa-dashboard"></i> Kategori</a></li>
			<li class="active">Edit kategori</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				Edit Kategori
			</div>
			<!-- /.box-header -->
			<?php 
			$id = $_GET['id'];
			$data = mysqli_query ($koneksi, "SELECT * FROM ds_evidences WHERE id = $id");
			while ($row = mysqli_fetch_array($data))
			{
				?>
				<form name="tambah" role="form" action="update.php" method="post" >
					<div class="box-body">
						<!-- text input -->
						<div class="form-group">
							<label>Kode Gejala</label>
							<input type="hidden" class="form-control" placeholder="Nama Gejala ..." name="id" value="<?php echo $row['id']; ?>">
							<input type="text" class="form-control" placeholder="Kode Gejala ..." name="code" value="<?php echo $row['code']; ?>" required>
						</div>
						<div class="form-group">
							<label>Nama Gejala</label>
							<input type="text" class="form-control" placeholder="Nama Gejala ..." name="name" value="<?php echo $row['name']; ?>" required>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</form>
			<?php } ?>
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>