    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left info">
            <p><?php echo $_SESSION['user']['nama']; ?></p>
          </div>

          <br>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li>
            <a href="index.php">
              <i class="fa fa-files-o"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Hama - Penyakit</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="kategori/kategori.php"><i class="fa fa-circle-o"></i> Kategori</a></li>
              <li><a href="hamapenyakit/hamapenyakit.php"><i class="fa fa-circle-o"></i> Hama - Penyakit</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-tasks" aria-hidden="true"></i><span>Rule Based</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="gejala/gejala.php"><i class="fa fa-circle-o"></i> Gejala</a></li>
              <li><a href="aturan/aturan.php"><i class="fa fa-circle-o"></i> Aturan</a></li>
            </ul>
          </li>
          <li>
            <a href="info/info.php">
              <i class="fa fa-files-o"></i>
              <span>Form Gejala</span>
            </a>
          </li>
          <li>
            <a href="history/history.php">
              <i class="fa fa-files-o"></i>
              <span>History</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>