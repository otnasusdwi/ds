<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data History
		</h4>
		<ol class="breadcrumb">
			<li><a href="history.php"><i class="fa fa-dashboard"></i> History</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>NIK</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>Gejala</th>
							<th>Penyakit</th>
							<th>Waktu</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						$data = mysqli_query ($koneksi, "SELECT * FROM  ds_history ORDER BY id_history DESC");
						while($row = mysqli_fetch_array($data))
						{
							?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row['nik']; ?></td>
								<td><?php echo $row['nama']; ?></td>
								<td><?php echo $row['alamat']; ?></td>
								<td><?php echo $row['evidence']; ?></td>
								<td><?php echo $row['problem']; ?></td>
								<td><?php echo $row['waktu']; ?></td>
							</tr>
							<?php $no++;}  ?>
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>NIK</th>
								<th>Nama</th>
								<th>Alamat</th>
								<th>Gejala</th>
								<th>Penyakit</th>
								<th>Waktu</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					Tata UAD 2019
				</div>
			</div>
			<!-- /.box -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
	</footer>
</div>
<!-- wrapper -->

<?php 
include ("../template/footer.php");
?>