<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4>
      Data Hama - Penyakit
    </h4>
    <ol class="breadcrumb">
      <li><a href="kategori.php"><i class="fa fa-dashboard"></i> Hama - Penyakit</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Data Kategori</h3> -->
        <a href="tambah_hamapenyakit.php" class="btn-success btn-sm">
          <i class="fa fa-plus"></i>&nbsp; Tambah
        </a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Hama - Penyakit</th>
              <th>Kategori</th>
              <th style="width: 40%; text-align: justify;">Solusi</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            $data = mysqli_query ($koneksi, "SELECT ds_problems.id, ds_category.category_name, ds_problems.code, ds_problems.name, ds_problems.solusi FROM ds_category INNER JOIN ds_problems ON ds_category.id_category=ds_problems.id_category ORDER BY ds_problems.id DESC ");
            while($row = mysqli_fetch_array($data))
            {
             ?>
             <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row['code']; ?></td>
              <td><?php echo $row['name']; ?></td>
              <td><?php echo $row['category_name']; ?></td>
              <td><?php echo $row['solusi']; ?></td>
              <td>
                <a href="edit_hamapenyakit.php?id=<?php echo $row['id']; ?>" class="btn-warning btn-sm">
                  <i class="fa fa-edit"></i> Edit
                </a> &nbsp;
                <a href="javascript:confirmDelete('delete.php?id=<?php echo $row['id']; ?>')" class="btn-danger btn-sm">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td>
            </tr>
            <?php $no++;}  ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>kode</th>
              <th>Hama - Penyakit</th>
              <th>Kategori</th>
              <th>Solusi</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Tata UAD 2019
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
  </footer>
</div>
<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>
