<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Hama - Penyakit
		</h4>
		<ol class="breadcrumb">
			<li><a href="hamapenyakit.php"><i class="fa fa-dashboard"></i> Hama - Penyakit</a></li>
			<li class="active">Tambah Hama - Penyakit</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Data Kategori</h3> -->
				Tambah Hama - Penyakit
			</div>
			<!-- /.box-header -->
			<form name="tambah" role="form" action="add.php" method="post" >
				<div class="box-body">
					<!-- text input -->
					<div class="form-group">
						<label>Kode</label>
						<input type="text" class="form-control" placeholder="Kode ..." name="code" required>
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" placeholder="Nama ..." name="name" required>
					</div>
					<div class="form-group">
						<label>Kategori</label>								
						<select name="id_category" class="form-control">
							<?php 
							$data = mysqli_query ($koneksi, "SELECT * FROM  ds_category ORDER BY id_category DESC");
							while($row = mysqli_fetch_array($data))
							{
								?>
								<option value="<?php echo $row['id_category'] ?>"><?php echo $row['category_name'] ?></option>
							<?php }  ?>
						</select>
					</div>
					<div class="form-group">
						<label>Solusi</label>
						<textarea class="form-control" name="solusi"></textarea>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>					
		</form>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->

<?php 
include ("../template/footer.php");
?>
