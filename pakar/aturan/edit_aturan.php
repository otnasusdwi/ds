<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Aturan
		</h4>
		<ol class="breadcrumb">
			<li><a href="hamapenyakit.php"><i class="fa fa-dashboard"></i> Aturan</a></li>
			<li class="active">Edit Aturan</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Data Kategori</h3> -->
				Edit Aturan
			</div>
			<!-- /.box-header -->
			<?php 
			$id = $_GET['id'];
			$data = mysqli_query ($koneksi, "SELECT * FROM ds_rules WHERE id = $id");
			while ($row = mysqli_fetch_array($data))
			{
				?>
				<form name="update" role="form" action="update.php" method="post" >
					<div class="box-body">
						<input type="hidden" class="form-control" name="id" value="<?php echo $row['id']; ?>">
						<div class="col-md-9">
							<div class="form-group">
								<label>Hama - Penyakit</label>								
								<select name="id_problem" class="form-control" required>
									<?php 
									$dataproblem = mysqli_query ($koneksi, "SELECT * FROM  ds_problems ORDER BY name ASC");
									while($problem = mysqli_fetch_array($dataproblem))
									{
										?>
										<option value="<?php echo $problem['id'] ?>" <?php	if($problem['id'] == $row['id_problem']){ echo "selected";}?>><?php echo $problem['name'] ?></option>
									<?php }  ?>
								</select>
							</div>
						</div>
						<div class="col-md-9">
							<div class="form-group">
								<label>Gejala</label>
								<select class="form-control" data-placeholder="Pilih Gejala" style="width: 100%;" name="id_evidence" required>
									<?php 
									$datagejala = mysqli_query ($koneksi, "SELECT * FROM  ds_evidences ORDER BY name ASC");
									while($gejala = mysqli_fetch_array($datagejala))
									{
										?>
										<option value="<?php echo $gejala['id'] ?>" <?php	if($gejala['id'] == $row['id_evidence']){ echo "selected";}?>><?php echo $gejala['name'] ?></option>
									<?php }  ?>
								</select>
							</div>
						</div>

						<div class="col-md-9">
							<div class="form-group">
								<label>Nilai Belief</label>
								<input type="text" class="form-control" placeholder="Nilai Belief ..." name="cf" required value="<?php echo $row['cf'] ?>">
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>					
			</form>
		<?php } ?>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>