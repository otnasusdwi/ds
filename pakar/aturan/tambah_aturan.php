<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h4>
			Data Aturan
		</h4>
		<ol class="breadcrumb">
			<li><a href="hamapenyakit.php"><i class="fa fa-dashboard"></i> Aturan</a></li>
			<li class="active">Tambah Aturan</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Data Kategori</h3> -->
				Tambah Aturan
			</div>
			<!-- /.box-header -->
			<form name="tambah" role="form" action="add.php" method="post" >
				<div class="box-body">
					<div class="col-md-9">
						<div class="form-group">
							<label>Hama - Penyakit</label>								
							<select name="id_problem" class="form-control" required>
								<?php 
								$data = mysqli_query ($koneksi, "SELECT * FROM  ds_problems ORDER BY name ASC");
								while($row = mysqli_fetch_array($data))
								{
									?>
									<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
								<?php }  ?>
							</select>
						</div>
					</div>
					<div id="newlink">
						<div class="col-md-6">
							<div class="form-group">
								<label>Gejala</label>
								<select class="form-control" data-placeholder="Pilih Gejala" style="width: 100%;" name="id_evidence[]" required>
									<?php 
									$data = mysqli_query ($koneksi, "SELECT * FROM  ds_evidences ORDER BY name ASC");
									while($row = mysqli_fetch_array($data))
									{
										?>
										<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
									<?php }  ?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Nilai Belief</label>
								<input type="text" class="form-control" placeholder="Nilai Belief ..." name="cf[]" required>
							</div>
						</div>							
					</div>
					<div class="col-md-9" id="addnew">	
						<span class="pull-right"><a href="javascript:new_link()" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp;Tambah Gejala</a></span>
					</div>	
				</div>

				<div class="box-footer">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>

			<!-- </form> -->
			<!-- Template -->
			<div id="newlinktpl" style="display:none">
				<div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Gejala</label>
							<select class="form-control" data-placeholder="Pilih Gejala" style="width: 100%;" name="id_evidence[]" required>
								<?php 
								$data = mysqli_query ($koneksi, "SELECT * FROM  ds_evidences ORDER BY name ASC");
								while($row = mysqli_fetch_array($data))
								{
									?>
									<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
								<?php }  ?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Nilai Belief</label>
							<input type="text" class="form-control" placeholder="Nilai Belief ..." name="cf[]" required>
						</div>
					</div>
				</div>
			</div>


		</div>					

		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	Tata &copy; 2019 | Universitas Ahmad Dahlan
</footer>
</div>
<script>
/*
This script is identical to the above JavaScript function.
*/
var ct = 1;
function new_link()
{
	ct++;
	var div1 = document.createElement('div');
	div1.id = ct;
	// link to delete extended form elements
	var delLink = '<div class="col-md-3"><br><a href="javascript:delIt('+ ct +')" class="btn btn-danger">Del</a></div>';
	div1.innerHTML = document.getElementById('newlinktpl').innerHTML + delLink;
	document.getElementById('newlink').appendChild(div1);
}
// function to delete the newly added set of elements
function delIt(eleId)
{
	d = document;
	var ele = d.getElementById(eleId);
	var parentEle = d.getElementById('newlink');
	parentEle.removeChild(ele);
}
</script>

<!-- ./wrapper -->
<?php 
include ("../template/footer.php");
?>