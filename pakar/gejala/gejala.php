<?php 
include ("../../lib/koneksi.php");
include ("../template/header.php");
include ("../template/sidebar.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h4>
      Data Gejala
    </h4>
    <ol class="breadcrumb">
      <li><a href="gejala.php"><i class="fa fa-dashboard"></i> Gejala</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-success">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">Data Kategori</h3> -->
        <a href="tambah_gejala.php" class="btn-success btn-sm">
          <i class="fa fa-plus"></i>&nbsp; Tambah
        </a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Nama Gejala</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            $data = mysqli_query ($koneksi, "SELECT * FROM  ds_evidences ORDER BY id DESC");
            while($row = mysqli_fetch_array($data))
            {
             ?>
             <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $row['code']; ?></td>
              <td><?php echo $row['name']; ?></td>
              <td>
                <a href="edit_gejala.php?id=<?php echo $row['id']; ?>" class="btn-warning btn-sm">
                  <i class="fa fa-edit"></i> Edit
                </a> &nbsp;
                <a href="javascript:confirmDelete('delete.php?id=<?php echo $row['id']; ?>')" class="btn-danger btn-sm">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td>
            </tr>
            <?php $no++;}  ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Nama Gejala</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        Tata UAD 2019
      </div>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Tata &copy; 2019 | Universitas Ahmad Dahlan</strong>
</footer>
</div>
<!-- ./wrapper --><?php 
include ("../template/footer.php");
?>