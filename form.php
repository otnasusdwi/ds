<?php 
include ("lib/koneksi.php");
include ("header.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Sistem Pakar
		</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- SELECT2 EXAMPLE -->
		<div class="box box-success">
			<div class="box-header with-border">
				Form Petani
			</div>
			<!-- /.box-header -->
			<form name="tambah" role="form" action="konsultasi.php" method="get" >
				<div class="box-body">
					<!-- text input -->
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" placeholder="Nama" name="nama" required>
					</div>					
					<div class="form-group">
						<label>NIK</label>
						<input type="number" class="form-control" placeholder="NIK..." name="nik" required>
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<input type="text" class="form-control" placeholder="Alamat" name="alamat" required>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Selanjutnya</button>
				</div>				
			</form>
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	<strong>Tata &copy; 2019 | Universitas Ahmad Dahlan
	</footer>
</div>
<!-- ./wrapper -->
<?php 
include ("footer.php");
?>
